class RenameCategoryToItems < ActiveRecord::Migration[5.0]
  def change
    rename_column :items, :id_category, :category_id
  end
end
