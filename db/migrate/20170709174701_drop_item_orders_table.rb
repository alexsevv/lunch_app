class DropItemOrdersTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :item_orders
  end
end
