class AddFirstDishSecondDishDrinkToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :first_dish, :integer
    add_column :orders, :second_dish, :integer
    add_column :orders, :drink, :integer
    change_column :items, :price, :integer
  end
end
