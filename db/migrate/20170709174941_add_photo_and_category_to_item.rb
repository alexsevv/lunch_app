class AddPhotoAndCategoryToItem < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :photo, :string
    rename_column :items, :category_id, :category
  end
end
