class ChangeTypeCategoryToItems < ActiveRecord::Migration[5.0]
  def change
    change_column :items, :category, :string
  end
end
