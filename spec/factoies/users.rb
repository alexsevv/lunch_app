FactoryGirl.define do
  factory :user do
    name { "User#{rand(999)}" }
    sequence(:email) { |n| "someguy_#{n}@example.com" }
    after(:build) { |u| u.password_confirmation = u.password = "123456" }
  end
end
