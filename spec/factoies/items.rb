FactoryGirl.define do
  factory :item do
    name { "Item#{rand(999)}" }
    price { "#{rand(999)}"}
    category { "#{['первое', 'второе', 'напиток'].sample}" }
    photo { "http://alexsevv-lunch.tw1.ru/uploads/item/photo/15/okroshka.jpg" }
  end
end
