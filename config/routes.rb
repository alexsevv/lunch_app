Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "items#index"

  resources :items
  resources :orders
  resources :users, only: [:show, :edit, :update, :index]

  namespace :api do
    resources :orders, only: [:index]
  end
end
