class Order < ApplicationRecord
  belongs_to :user
  has_many :items

  validates :user, presence: true

  validates :first_dish, presence: true
  validates :second_dish, presence: true
  validates :drink, presence: true
end
