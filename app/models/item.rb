class Item < ApplicationRecord
  has_many :orders
  has_many :users

  validates :name, presence: true, length: {maximum: 50}
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :category, presence: true, length: {maximum: 50}

  mount_uploader :photo, PhotoUploader
end
