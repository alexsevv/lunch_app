class Api::OrdersController < ApplicationController

  def index
    orders = Order.all
    render json: orders.to_json(:include => {:user => {
                                              :only => [:email, :name] } },
                                              :only => [:created_at, :first_dish, :second_dish, :drink] )
  end

end


# konata.to_json(:include => { :posts => {
                              #   :include => { :comments => {
                               #                :only => :body } },
                              #   :only => :title } })
  # => {"id": 1, "name": "Konata Izumi", "age": 16,
       # "created_at": "2006/08/01", "awesome": true,
       # "posts": [{"comments": [{"body": "1st post!"}, {"body": "Second!"}],
        #           "title": "Welcome to the weblog"},
         #         {"comments": [{"body": "Don't think too hard"}],
         #          "title": "So I was thinking"}]}


 #<td><%= Item.find(order.first_dish).name %></td>
